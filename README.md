# Webapp Composer

Are you on the Internet as much as everyone else? Maybe just a tad bit more?? [Webapp Composer](https://gitlab.com/alex_ford/webapp-composer) aims to give you another tool in your arsenal to increase your productivity and manage your screen real-estate effectively!

Any "webapp", or web application, can be composed into a layout with other webapps. This allows you to create custom web pages made up of just the webapps you want/need.

![Screenshot of the Chrome appified version.](img\screenshot-20180611T1014.png)

## Current State

See [docs\CURRENT_STATUS.md](docs\CURRENT_STATUS.md) for detailed maturity assessment and feature status.

## Purpose

Why make this project?

Simply put, I've been trying out similar ideas for awhile, and just haven't found a solution that I like which remains simple, usable, and ultimately, free. I'm also always on the hunt for techniques and tools to enhance my productivity.

## License

See LICENSE for more details.

In essence, under the MIT License, you are free to use this software in any way you see fit. Enjoy!

## Getting Started

Section Updated: 2018-06-15

1. Clone this git repo to your local computer.
2. Make either a batch or powershell script on your Desktop, or somewhere convenient for you.
3. In that script, call the ./launch-as-chrome-app.ps1 Powershell script and pass in the URLs of the webapps you want to use.
4. Enjoy! (These steps will likely get slight more complicated as more features are added!)

Sample Powershell run script:
```powershell
& "C:\path\to\repo\webapp-composer\launch-as-chrome-app.ps1" "https://www.google.com" "https://www.yahoo.com"
```

## Limitations

### X Frame Options disallow cross-site embedding

Some websites use a special header which causes them to not be able to be embedded into another website/webpage. This can be circumvented by installing a browser add-on which modifies the incoming header and removes the particular one which causes this. At this time, this project does *not* address this issue directly and is left up to the user to resolve.

## Third-party Attribution

- W3Schools for their comprehensive, yet easy to use, CSS styling framework.

## Contributing

It would be great to collaborate on this project with others! Simply fork this git repo, make your changes, and submit them as a merge request. When I get the chance, I will take a look at what you're proposing and if it's aligned with the project's vision, we'll work together to incorporate your work!

## Noteworthy Mentions

### Freeter

A desktop app which essentially gives you a window that you can insert webapps/web pages, lists, file explorers, etc. Also has some other nice features like timers, and customizable search boxes.

### Station

A desktop app similar to Freeter.

### Rambox

Yet another desktop app similar to Freeter and Station.
