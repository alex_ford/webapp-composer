# Ideas

## Alerts and notifications

It's possible to have webapps present useful alerts even when they are in the background. Web apps that already do this will likely just continue to do this when added to Webapp Composer. But what about custom alerts/events? Is there a way, and a need, to allow users to create custom alerts based on their app compositions?