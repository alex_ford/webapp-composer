# ATTRIBUTION STATEMENTS

The following are the "canned" statements which should be placed as comments above the respective source files.

## HTML

```html
<!--
    Copyright (c) 2018 Alex Richard Ford.
    Licensed under the MIT License, a permissive license popular among open source projects.
    Webapp Composer website: https://gitlab.com/alex_ford/webapp-composer
    Developer website: http://blog.alexrichardford.com
-->
```

## Powershell

```powershell
# Copyright (c) 2018 Alex Richard Ford.
# Licensed under the MIT License, a permissive license popular among open source projects.
# Webapp Composer website: https://gitlab.com/alex_ford/webapp-composer
# Developer website: http://blog.alexrichardford.com
```