# CURRENT STATUS

Look here to see where the project currently is in terms of maturity and features.

Maturity Level: 2 / 10
> Useful for power users and other developers. You will most likely have to edit the code in order to make it do what you want. A lot of planned features are still missing.

## Features

### Set webapp URLs through URL Query Parameters

> As of 2018-06-13

When launching the index.html in your favorite browser (Chrome?), you now need to also pass in the Webapp URLs you wish to use. At the moment, `app1` and `app2` are the only available ones. More will be supported in the future, in addition to the ability to edit URLs from Webapp Composer page itself.

Example:

```html
index.html?app1=https://www.ticktick.com&app2=https://www.timecamp.com/app%23/timesheets/timer
```

### Responsive width and height of webapp iframes

As of 2018-06-11

When you resize the window, or use different devices with different screen sizes, Webapp Composer will simply respond by shrinking or growing the available size given to the webapps in their iframes. NOTE: nothing special is currently done to the actual webapp itself, so if you don't give it enough space (or perhaps if there is too much space), then you will likely have rendering issues.

### Open as a Chrome desktop app

As of 2018-06-11

> This provides a better look-and-feel in terms of integraing with the user's desktop. This makes Webapp Composer look like any other application the user may be running. The `launch-as-chrome-app.ps1` takes care of this.

### Display two webapps side-by-side in a column layout

> As of 2018-06-10

Support for displaying two webapps is implemented! Just set the URLs on the iframes in index.html and you're good to go!

## Known Bugs/Issues

- You must escape certain characters in the URL when you pass them in as query parameters. Failure to do so will result in partial or broken webapps being displayed. One example is the anchor/hash/pound character "#" which must be converted to %23 in the URL in order for it to work. See https://en.wikipedia.org/wiki/Percent-encoding for a mapping of characters to their codes.