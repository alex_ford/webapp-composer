# Copyright (c) 2018 Alex Richard Ford.
# Licensed under the MIT License, a permissive license popular among open source projects.
# Webapp Composer website: https://gitlab.com/alex_ford/webapp-composer
# Developer website: http://blog.alexrichardford.com

# Simple script which opens the Webapp Composer as a "Chrome desktop app"

# This either accepts space separated URLs as parameters to use for the webapps, or
# loads in some defaults to give you an example of what it looks like with webapps
# fully loaded.

param (
    [string]$app1 = "https://www.ticktick.com",
    [string]$app2 = "https://www.toggl.com/app/timer",
    [string]$app3 = "https://www.timecamp.com/app%23/timesheets/timer"
)

Write-Output "Opening Webapp Composer for the following:"
Write-Output "    app1 = ${app1}"
Write-Output "    app2 = ${app2}"
Write-Output "    app3 = ${app3}"

Push-Location $PSScriptRoot

# NOTE: if there is a hash character ('#') in your URL, you must replace it with
# the '%23' code for it to work!
#
# NOTE 2: the --auto-open-devtools-for-tabs can be useful, but requires you to completely
# restart all running instances of Chrome to toggle on/off
#
# NOTE 3: not all websites can be iframed - some sites, like Google Keep for example,
# use a security feature named X-Frame-Options=SAMEORIGIN which protects them from
# being loaded by other sites/pages.
#
# A description of the flags chosen here:
#  --allow-insecure-localhost    Enables TLS/SSL errors on localhost to be ignored (no interstitial, no blocking of requests).
#  --disable-web-security    this allows Chrome to disable sites that set X-Frame-Options=SAMEORIGIN restriction
#  --user-data-dir=c:\my\data    must be used in conjunction with the above to get past the X-Frame-Options restriction
& "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" `
    --allow-insecure-localhost `
    --disable-web-security `
    --user-data-dir=c:\my\data `
    --app=$("file:\\$($(Get-Location).Path)\index.html?" + `
    "app1=${app1}&" + `
    "app2=${app2}&" + `
    "app3=${app3}")

Pop-Location